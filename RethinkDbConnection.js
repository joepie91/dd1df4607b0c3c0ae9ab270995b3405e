var Promise = require('bluebird');
var rdb = Promise.promisify(require('rethinkdb'));

module.exports = function(config) {
    return {
        connectToDb: function(){
            return Promise.try(() => {
                return rdb.connect(config);
            });
        }
    }
}