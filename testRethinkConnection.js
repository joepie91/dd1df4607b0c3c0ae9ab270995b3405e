var Promise = require('bluebird');
var rdb = require('rethinkdb');
var config = { host: "localhost", port: 28015 }
var RethinkDbConnection = require('./lib/RethinkDbConnection')(config).connectToDb();
var tableName = "testdb"; // <------ just for testing

var createTable = function(tableName){
    return Promise.try(() => {
        return RethinkDbConnection;
    }).then((connectionResult) => {
        return rdb.db('test').tableCreate(tableName).run(connectionResult);
    }).then((tableCreationResult) => {
        console.log(JSON.stringify(tableCreationResult, null, 2));
    });
}

createTable(tableName);